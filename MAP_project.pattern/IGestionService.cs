﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MAP_project.pattern
{
    public interface IGestionService<T> where T:class
    {
        void Ajouter(T entity);
        void Commit();
        void Supprimer(T entity);
        void MiseAJour(T entity);
        //        IEnumerable<T> GetAll(); avec condition = null on a fait deux methodes on une seule
        T RetournerSelonId(int id);
        T RetournerSelonId(string id);
        IEnumerable<T> RetournerSelonCondition(Expression<Func<T, bool>> condition = null, Expression<Func<T, bool>> orderBy = null); //4methodes on une seule
        void SupprimerSelonCondition(Expression<Func<T, bool>> condition);
    }
}
