﻿using MAP_project.data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAP_project.pattern
{
    public class GestionService<T> :IGestionService<T> where T:class

    {
        public UnitOfWork unitOfWork = null;

        public GestionService(UnitOfWork utw)
        {

            unitOfWork = utw;


        }

        public void Ajouter(T entity)
        {
            unitOfWork.getRepository<T>().Add(entity);
        }
        public void Commit()
        {
            unitOfWork.Commit();
        }


        public void MiseAJour(T entity)
        {
            unitOfWork.getRepository<T>().Update(entity);
        }

        public IEnumerable<T> RetournerSelonCondition(System.Linq.Expressions.Expression<Func<T, bool>> condition = null, System.Linq.Expressions.Expression<Func<T, bool>> orderBy = null)
        {
            return unitOfWork.getRepository<T>().GetByCondition(condition, orderBy);
        }

        public T RetournerSelonId(string id)
        {
            return unitOfWork.getRepository<T>().GetById(id);
        }

        public T RetournerSelonId(int id)
        {
            return unitOfWork.getRepository<T>().GetById(id);
        }

        public void Supprimer(T entity)
        {
            unitOfWork.getRepository<T>().Remove(entity);
        }

        public void SupprimerSelonCondition(System.Linq.Expressions.Expression<Func<T, bool>> condition)
        {
            unitOfWork.getRepository<T>().Remove(condition);
        }
    }
}
