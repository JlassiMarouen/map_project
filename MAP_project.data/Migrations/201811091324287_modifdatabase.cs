namespace MAP_project.data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifdatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ApplicantForms",
                c => new
                    {
                        ApplicantFormId = c.Int(nullable: false, identity: true),
                        testResult = c.String(),
                    })
                .PrimaryKey(t => t.ApplicantFormId);
            
            CreateTable(
                "dbo.JobRequests",
                c => new
                    {
                        JobRequestId = c.Int(nullable: false, identity: true),
                        idApplicant = c.String(maxLength: 128),
                        date = c.DateTime(nullable: false),
                        specialite = c.String(),
                        state = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.JobRequestId)
                .ForeignKey("dbo.AspNetUsers", t => t.idApplicant)
                .Index(t => t.idApplicant);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        name = c.String(),
                        surname = c.String(),
                        password = c.String(nullable: false, maxLength: 15),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                        category_type = c.Int(),
                        logo = c.String(),
                        client_type = c.Int(),
                        picture = c.String(),
                        profile = c.String(),
                        seniority = c.Int(),
                        note = c.String(),
                        contract = c.Int(),
                        sector = c.String(),
                        state = c.Int(),
                        MprojectId = c.String(),
                        cv = c.String(),
                        origine = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        MyJobRequest_JobRequestId = c.Int(),
                        myOrganigram_OrganigramId = c.Int(),
                        MyResourceManager_Id = c.String(maxLength: 128),
                        MyResourceRequestes_ResourceRequestId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.JobRequests", t => t.MyJobRequest_JobRequestId)
                .ForeignKey("dbo.Organigrams", t => t.myOrganigram_OrganigramId)
                .ForeignKey("dbo.AspNetUsers", t => t.MyResourceManager_Id)
                .ForeignKey("dbo.ResourceRequests", t => t.MyResourceRequestes_ResourceRequestId)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex")
                .Index(t => t.MyJobRequest_JobRequestId)
                .Index(t => t.myOrganigram_OrganigramId)
                .Index(t => t.MyResourceManager_Id)
                .Index(t => t.MyResourceRequestes_ResourceRequestId);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        MessageId = c.Int(nullable: false, identity: true),
                        TypeMessage = c.Int(nullable: false),
                        cible = c.Int(nullable: false),
                        dateM = c.DateTime(nullable: false),
                        etatM = c.String(),
                        suivi = c.Int(nullable: false),
                        MyClient_Id = c.String(maxLength: 128),
                        MyResource_Id = c.String(maxLength: 128),
                        Client_Id = c.String(maxLength: 128),
                        Resource_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.MessageId)
                .ForeignKey("dbo.AspNetUsers", t => t.MyClient_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.MyResource_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.Client_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.Resource_Id)
                .Index(t => t.MyClient_Id)
                .Index(t => t.MyResource_Id)
                .Index(t => t.Client_Id)
                .Index(t => t.Resource_Id);
            
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        ProjectId = c.Int(nullable: false, identity: true),
                        type = c.String(),
                        deb_Date = c.DateTime(nullable: false),
                        fin_Date = c.DateTime(nullable: false),
                        NbreRessources = c.Int(nullable: false),
                        idClient = c.String(maxLength: 128),
                        myOrganigram_OrganigramId = c.Int(),
                        Client_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ProjectId)
                .ForeignKey("dbo.AspNetUsers", t => t.idClient)
                .ForeignKey("dbo.Organigrams", t => t.myOrganigram_OrganigramId)
                .ForeignKey("dbo.AspNetUsers", t => t.Client_Id)
                .Index(t => t.idClient)
                .Index(t => t.myOrganigram_OrganigramId)
                .Index(t => t.Client_Id);
            
            CreateTable(
                "dbo.Organigrams",
                c => new
                    {
                        OrganigramId = c.Int(nullable: false, identity: true),
                        nameProgram = c.String(),
                        nameCh = c.String(),
                        nameProject = c.String(),
                        nameClient = c.String(),
                        accountManager = c.String(),
                        nameAssignementM = c.String(),
                    })
                .PrimaryKey(t => t.OrganigramId);
            
            CreateTable(
                "dbo.ResourceRequests",
                c => new
                    {
                        ResourceRequestId = c.String(nullable: false, maxLength: 128),
                        clientRRId = c.String(maxLength: 128),
                        state_type = c.Int(nullable: false),
                        resourceType = c.String(),
                        Duration = c.String(),
                        coast = c.Single(nullable: false),
                        project = c.String(),
                    })
                .PrimaryKey(t => t.ResourceRequestId)
                .ForeignKey("dbo.AspNetUsers", t => t.clientRRId)
                .Index(t => t.clientRRId);
            
            CreateTable(
                "dbo.MandateHistories",
                c => new
                    {
                        MandateHistoryId = c.Int(nullable: false, identity: true),
                        saveDate = c.DateTime(nullable: false),
                        idMandat = c.String(),
                    })
                .PrimaryKey(t => t.MandateHistoryId);
            
            CreateTable(
                "dbo.Mandates",
                c => new
                    {
                        MandateId = c.Int(nullable: false, identity: true),
                        startDate = c.DateTime(nullable: false),
                        endDate = c.DateTime(nullable: false),
                        fee = c.Single(nullable: false),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        myProject_ProjectId = c.Int(),
                        myResource_Id = c.String(maxLength: 128),
                        MyHistory_MandateHistoryId = c.Int(),
                    })
                .PrimaryKey(t => t.MandateId)
                .ForeignKey("dbo.Projects", t => t.myProject_ProjectId)
                .ForeignKey("dbo.AspNetUsers", t => t.myResource_Id)
                .ForeignKey("dbo.MandateHistories", t => t.MyHistory_MandateHistoryId)
                .Index(t => t.myProject_ProjectId)
                .Index(t => t.myResource_Id)
                .Index(t => t.MyHistory_MandateHistoryId);
            
            CreateTable(
                "dbo.Skils",
                c => new
                    {
                        SkilsId = c.Int(nullable: false, identity: true),
                        idRes = c.Int(nullable: false),
                        idProject = c.Int(nullable: false),
                        value = c.String(),
                    })
                .PrimaryKey(t => t.SkilsId);
            
            CreateTable(
                "dbo.Tests",
                c => new
                    {
                        TestId = c.Int(nullable: false, identity: true),
                        stateTest = c.Int(nullable: false),
                        date = c.DateTime(nullable: false),
                        testType = c.Int(nullable: false),
                        TestResult = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TestId);
            
            CreateTable(
                "dbo.Vacations",
                c => new
                    {
                        VacationId = c.Int(nullable: false, identity: true),
                        startDate = c.DateTime(nullable: false),
                        endDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.VacationId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.ResourceProjects",
                c => new
                    {
                        Resource_Id = c.String(nullable: false, maxLength: 128),
                        Project_ProjectId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Resource_Id, t.Project_ProjectId })
                .ForeignKey("dbo.AspNetUsers", t => t.Resource_Id, cascadeDelete: true)
                .ForeignKey("dbo.Projects", t => t.Project_ProjectId, cascadeDelete: true)
                .Index(t => t.Resource_Id)
                .Index(t => t.Project_ProjectId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Mandates", "MyHistory_MandateHistoryId", "dbo.MandateHistories");
            DropForeignKey("dbo.Mandates", "myResource_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Mandates", "myProject_ProjectId", "dbo.Projects");
            DropForeignKey("dbo.JobRequests", "idApplicant", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "MyResourceRequestes_ResourceRequestId", "dbo.ResourceRequests");
            DropForeignKey("dbo.ResourceRequests", "clientRRId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Projects", "Client_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Projects", "myOrganigram_OrganigramId", "dbo.Organigrams");
            DropForeignKey("dbo.AspNetUsers", "MyResourceManager_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.ResourceProjects", "Project_ProjectId", "dbo.Projects");
            DropForeignKey("dbo.ResourceProjects", "Resource_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "myOrganigram_OrganigramId", "dbo.Organigrams");
            DropForeignKey("dbo.Messages", "Resource_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Projects", "idClient", "dbo.AspNetUsers");
            DropForeignKey("dbo.Messages", "Client_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Messages", "MyResource_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Messages", "MyClient_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "MyJobRequest_JobRequestId", "dbo.JobRequests");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.ResourceProjects", new[] { "Project_ProjectId" });
            DropIndex("dbo.ResourceProjects", new[] { "Resource_Id" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Mandates", new[] { "MyHistory_MandateHistoryId" });
            DropIndex("dbo.Mandates", new[] { "myResource_Id" });
            DropIndex("dbo.Mandates", new[] { "myProject_ProjectId" });
            DropIndex("dbo.ResourceRequests", new[] { "clientRRId" });
            DropIndex("dbo.Projects", new[] { "Client_Id" });
            DropIndex("dbo.Projects", new[] { "myOrganigram_OrganigramId" });
            DropIndex("dbo.Projects", new[] { "idClient" });
            DropIndex("dbo.Messages", new[] { "Resource_Id" });
            DropIndex("dbo.Messages", new[] { "Client_Id" });
            DropIndex("dbo.Messages", new[] { "MyResource_Id" });
            DropIndex("dbo.Messages", new[] { "MyClient_Id" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", new[] { "MyResourceRequestes_ResourceRequestId" });
            DropIndex("dbo.AspNetUsers", new[] { "MyResourceManager_Id" });
            DropIndex("dbo.AspNetUsers", new[] { "myOrganigram_OrganigramId" });
            DropIndex("dbo.AspNetUsers", new[] { "MyJobRequest_JobRequestId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.JobRequests", new[] { "idApplicant" });
            DropTable("dbo.ResourceProjects");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Vacations");
            DropTable("dbo.Tests");
            DropTable("dbo.Skils");
            DropTable("dbo.Mandates");
            DropTable("dbo.MandateHistories");
            DropTable("dbo.ResourceRequests");
            DropTable("dbo.Organigrams");
            DropTable("dbo.Projects");
            DropTable("dbo.Messages");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.JobRequests");
            DropTable("dbo.ApplicantForms");
        }
    }
}
