﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using MAP_project.domaine.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace MAP_project.data
{
   public class MapContext :IdentityDbContext<ApplicationUser>
    {

        public MapContext()
            : base("MAP_PROJECT_PI", throwIfV1Schema: false)
        {
        }

        public static MapContext Create()
        {
            return new MapContext();
        }

        //les DBset
        public DbSet<Mandate> MyMandates { get; set; }
       // public DbSet<Client> MyClient { get; set; }
       // public DbSet<Administration> MyAdmin { get; set; }
       // public DbSet<Resource> MyRessource { get; set; }
       // public DbSet<Applicant> MyApplicant { get; set; }
        //public DbSet<ResourceManager> MyRessourceManager { get; set; }
        public DbSet<MandateHistory> MyMandateHistories { get; set; }

        public DbSet<ApplicantForm> MyApplicantForm { get; set; }
        public DbSet<JobRequest> MyJobRequestes { get; set; }
        public DbSet<Message> MyMessages { get; set; }

        public DbSet<Organigram> MyOrganigrams { get; set; }
        public DbSet<Project> MyProjects { get; set; }
        public DbSet<ResourceRequest> MyResourceRequests { get; set; }
        public DbSet<Skils> MySkils { get; set; }
        public DbSet<Test> MyTests { get; set; }
        //public DbSet<User> MyUsers { get; set; }
        public DbSet<Vacation> MyVacations { get; set; }

    }
}
