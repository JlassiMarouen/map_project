﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAP_project.data.Infrastructure
{
   public interface IdatabaseFactory
    {
        MapContext MyContext { get; }
    }
}
