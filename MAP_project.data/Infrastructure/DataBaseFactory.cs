﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAP_project.data.Infrastructure
{
    public class DataBaseFactory : IdatabaseFactory
    {
        private MapContext ctx = null;
        public DataBaseFactory()
        {
            ctx = new MapContext();
        }
        public MapContext MyContext
        {
            get
            {
                return ctx;

            }
        }
    }
}
