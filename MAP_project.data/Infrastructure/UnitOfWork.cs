﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAP_project.data.Infrastructure
{
    public class UnitOfWork: IUnitOfWork,IDisposable
    {
        private DataBaseFactory dbFactory = null;
        public UnitOfWork(DataBaseFactory dbfactory)
        {
            this.dbFactory = dbfactory;

        }
        public MapContext Ctx
        {
            get { return dbFactory.MyContext; }
        }
        public void Commit()
        {
            //   dbFactory.MyContext.SaveChanges();  on peut faire comme sa ou comme sa
            Ctx.SaveChanges();
        }

        public void Dispose()
        {
            if (Ctx != null)
                Ctx.Dispose();
        }

        public RepositoryBase<T> getRepository<T>() where T : class
        {
            return new RepositoryBase<T>(dbFactory);
        }
    }
}
