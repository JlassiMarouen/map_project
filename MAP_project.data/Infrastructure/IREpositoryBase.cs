﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MAP_project.data.Infrastructure
{
   public interface IREpositoryBase<T> where T : class
    {
        void Add(T entity);
        void Remove(T entity);
        void Update(T entity);
        //        IEnumerable<T> GetAll(); avec condition = null on a fait deux methodes on une seule
        T GetById(int id);
        T GetById(string id);
        IEnumerable<T> GetByCondition(Expression<Func<T, bool>> condition = null, Expression<Func<T, bool>> orderBy = null); //4methodes on une seule
        void Remove(Expression<Func<T, bool>> condition);
    }
}
