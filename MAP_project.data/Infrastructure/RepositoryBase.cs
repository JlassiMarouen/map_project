﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MAP_project.data.Infrastructure
{
    public class RepositoryBase<T> : IREpositoryBase<T> where T:class
    {
        //private MyFinnanceContext ctx;
        public DataBaseFactory dbfactory = null;
        private DbSet<T> dbset = null;


        public RepositoryBase(DataBaseFactory db)
        {
            this.dbfactory = db;
            dbset = Ctx.Set<T>();

        }
        public MapContext Ctx
        {
            get
            {
                return dbfactory.MyContext;
            }
        }
        public void Add(T entity)
        {
            dbset.Add(entity);
        }

        public IEnumerable<T> GetByCondition(Expression<Func<T, bool>> condition = null, Expression<Func<T, bool>> orderBy = null)
        {
            if ((condition != null) && (orderBy != null))

                return dbset.Where(condition).OrderBy(orderBy);
            else
            if (condition != null)

                return dbset.Where(condition);
            else
                if (orderBy != null)

                return dbset.OrderBy(orderBy);
            else

                return dbset;

        }

        public T GetById(string id)
        {
            return dbset.Find(id);
        }

        public T GetById(int id)
        {
            return dbset.Find(id);
        }

        public void Remove(Expression<Func<T, bool>> condition)
        {
            IEnumerable<T> MaListe = dbset.Where(condition);
            foreach (var item in MaListe)
                dbset.Remove(item);
        }

        public void Remove(T entity)
        {
            dbset.Remove(entity);
        }

        public void Update(T entity)
        {
            dbset.Attach(entity);
            Ctx.Entry(entity).State = EntityState.Modified;
        }
    }
}
