﻿using MAP_project.domaine.Entity;
using MAP_project.web.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MAP_project.web.Startup))]
namespace MAP_project.web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            CreateRoles();
            //CreateUsers();
        }
        //public void CreateUsers()
        //{
        //    var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
        //    var user = new ApplicationUser();
        //    user.Email = "khiari55.donia@esprit.tn";
        //    user.UserName = "Donia564";
        //    var check = userManager.Create(user, "A@z20071");
        //    if (check.Succeeded)
        //    {
        //        userManager.AddToRole(user.Id, "Admin");
        //    }
        //}

        public void CreateRoles()
        {
            ApplicationDbContext db = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            IdentityRole role ;
            if (!roleManager.RoleExists("Admin"))
            {
                role = new IdentityRole();
                role.Name = "Admin";
                roleManager.Create(role);
            }


            if (!roleManager.RoleExists("Client"))
            {
                role = new IdentityRole();
                role.Name = "Client";
                roleManager.Create(role);
            }
            if (!roleManager.RoleExists("RessourceManager"))
            {
                role = new IdentityRole();
                role.Name = "RessourceManager";
                roleManager.Create(role);
            }
            if (!roleManager.RoleExists("Ressource"))
            {
                role = new IdentityRole();
                role.Name = "Ressource";
                roleManager.Create(role);
            }

        }
    }
}
