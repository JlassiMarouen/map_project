﻿using MAP_project.domaine.Entity;
using MAP_project.service;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MAP_project.web.Controllers
{
    public class CompetenceController : Controller
    {

        GestionCompetences gestComp = new GestionCompetences();
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: Competence
        public ActionResult Index()
        {
            var liste = gestComp.RetournerSelonCondition();
            return View(liste);
        }

        // GET: Competence/Details/5
        public ActionResult Details(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }
            Skils competence = db.MySkils.Find(id);
            if (competence == null)
            {
                return HttpNotFound();
            }
            return View(competence);
        }

        // GET: Competence/Create
        public ActionResult CreateCompetence()
        {
            return View();
        }

        // POST: Competence/Create
        [HttpPost]
        public ActionResult CreateCompetence(Skils c)
        {
            if (ModelState.IsValid)
            {
                gestComp.Ajouter(c);
                gestComp.Commit();
                return RedirectToAction("Index");
            }
            return View(c);
        }

        // GET: Competence/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }
            Skils competence = db.MySkils.Find(id);
            if (competence == null)
            {
                return HttpNotFound();
            }
            return View(competence);
        }

        // POST: Competence/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SkilsId,value")]Skils competence)
        {
            if (ModelState.IsValid)
            {
                db.Entry(competence).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(competence);
        }

        // GET: Competence/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }
            Skils competence = db.MySkils.Find(id);
            if (competence == null)
            {
                return HttpNotFound();
            }
            return View(competence);
        }

        // POST: Competence/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            Skils competence = db.MySkils.Find(id);
            db.MySkils.Remove(competence);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
