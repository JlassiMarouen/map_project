﻿using MAP_project.domaine.Entity;
using MAP_project.service;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MAP_project.web.Controllers
{
    public class ProjetController : Controller
    {
        GestionProjets gestproj = new GestionProjets();
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: Projet
        public ActionResult Index()
        {
            var liste = gestproj.RetournerSelonCondition();
            return View(liste);
        }

        // GET: Projet/Details/5
        public ActionResult Details(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }
           Project projet = db.MyProjects.Find(id);
            if (projet == null)
            {
                return HttpNotFound();
            }
            return View(projet);
        }

        // GET: Projet/Create
        public ActionResult CreateProjet()
        {
            return View();
        }

        // POST: Projet/Create
        [HttpPost]
        public ActionResult CreateProjet(Project p)
        {
            if (ModelState.IsValid)
            {
                gestproj.Ajouter(p);
                gestproj.Commit();
              return  RedirectToAction("Index");
            }
            return View(p);
        }

        // GET: Projet/Edit/5
        public ActionResult Edit(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }
            Project projet = db.MyProjects.Find(id);
            if (projet == null)
            {
                return HttpNotFound();
            }
            return View(projet);
        }

        // POST: Projet/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProjectId,type,deb_Date,fin_Date,NbreRessources")]Project projet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(projet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(projet);
        }

        // GET: Projet/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }
            Project projet = db.MyProjects.Find(id);
            if (projet == null)
            {
                return HttpNotFound();
            }
            return View(projet);
        }

        // POST: Projet/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            Project projet = db.MyProjects.Find(id);
            db.MyProjects.Remove(projet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
