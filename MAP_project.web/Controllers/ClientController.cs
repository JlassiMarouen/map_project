﻿using MAP_project.domaine.Entity;
using MAP_project.service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MAP_project.web.Controllers
{
    public class ClientController : Controller
    {
        GestionClients gestClient = new GestionClients();
        // GET: Client
        public ActionResult Index()
        {
            return View();
        }

        // GET: Client/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Client/Create
        public ActionResult CreateClient()
        {
            return View();
        }

        // POST: Client/Create
        [HttpPost]
        public ActionResult CreateClient(Client c)
        {
            if (ModelState.IsValid)
            {
                gestClient.Ajouter(c);
                gestClient.Commit();
                RedirectToAction("Index");
            }
            return View(c);
        }

        // GET: Client/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Client/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Client/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Client/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
