﻿using MAP_project.domaine.Entity;
using MAP_project.service;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MAP_project.web.Controllers
{
    public class RessourceController : Controller
    {
        GestionResource gestRes = new GestionResource();
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Ressource
        public ActionResult Index()
        {
            var liste = gestRes.RetournerSelonCondition();
            return View(liste);
         
        }

        // GET: Ressource/Details/5
        public ActionResult Details(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }
            Resource resource = db.MyRessource.Find(id);
            if (resource == null)
            {
                return HttpNotFound();
            }
            return View(resource);
        }

        // GET: Ressource/Create
        public ActionResult CreateRessource()
        {
            return View();
        }

        // POST: Ressource/Create
        [HttpPost]
        public ActionResult CreateRessource(Resource r)
        {
            if (ModelState.IsValid)
            {
                gestRes.Ajouter(r);
                gestRes.Commit();
                return  RedirectToAction("Index");


            }

            return View(r);
        }

        // GET: Ressource/Edit/5
        public ActionResult Edit(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }
            Resource resource = db.MyRessource.Find(id);
            if (resource == null)
            {
                return HttpNotFound();
            }
            return View(resource);
        }

        // POST: Ressource/Edit/5
        [HttpPost]
        public ActionResult Edit([Bind(Include ="ResourceId,picture,profile,cv,state,note,seniority")]Resource resource)
        {
            if (ModelState.IsValid)
            {
                db.Entry(resource).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(resource);
        }

        // GET: Ressource/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }
            Resource resource = db.MyRessource.Find(id);
            if (resource == null)
            {
                return HttpNotFound();
            }
            return View(resource);
        }

        // POST: Ressource/Delete/5
        [HttpPost,ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            Resource resource = db.MyRessource.Find(id);
            db.MyRessource.Remove(resource);
            db.SaveChanges();
            return RedirectToAction("Index");


        }
  
    }
}
