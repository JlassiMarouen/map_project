﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MAP_project.domaine.Entity
{
    public class Message
    {
       
        public int MessageId { get; set; }
        //[ForeignKey("MyResource")]
        //public string idResource { get; set; }

        //[ForeignKey("MyClient")]
        //public string idClient { get; set; }
        public MessageT TypeMessage { get; set; }
        public CibleT cible { get; set; }
        public DateTime dateM { get; set; }
        public string etatM { get; set; }

        public SuiviT suivi { get; set; }

        public virtual ApplicationUser MyClient { get; set; }

        public virtual ApplicationUser MyResource { get; set; }

    }
}
