﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MAP_project.domaine.Entity
{
    public class MandateHistory
    {
       
        public int MandateHistoryId { get; set; }
        [DataType(DataType.Date)]
        public DateTime saveDate { get; set; }
      [ForeignKey("MyMandates")]
        public string idMandat { get; set; }
        
        public virtual ICollection<Mandate> MyMandates { get; set; }
    }
}
