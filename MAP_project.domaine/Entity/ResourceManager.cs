﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace MAP_project.domaine.Entity
{
    public class ResourceManager : ApplicationUser
    {


        //[DataType(DataType.EmailAddress)]
        //public string email { get; set; }
        //public string phone { get; set; }
        [Required(ErrorMessage = "obligatoire")]
        [StringLength(15, ErrorMessage = "La chaine ne doit pas dépasser 15 caractéres")]
        [MinLength(5)]
        public string password { get; set; }
        public virtual ICollection<Resource> MyResources { get; set; }

    }
}
