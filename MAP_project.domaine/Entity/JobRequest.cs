﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MAP_project.domaine.Entity
{
   public  class JobRequest
    {
        
        public int JobRequestId { get; set; }

        [ForeignKey("MyApplicant")]
        public string idApplicant { get; set; }
        [DataType(DataType.Date)]
        public DateTime date { get; set; }

        public string specialite { get; set; }
        public RequestType state { get; set; }

        public virtual ApplicationUser MyApplicant { get; set; }

    }
}
