﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MAP_project.domaine.Entity
{
    public class Mandate
    {
        
        public int MandateId { get; set; }
        [DataType(DataType.Date)]
        public DateTime startDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime endDate { get; set; }
        public float fee { get; set; }
        //[ForeignKey("myResource")]
        //public string idResource { get; set; }
        //[ForeignKey("myProject")]
        //public string ProjectId { get; set; }
        public virtual MandateHistory MyHistory { get; set; }
        public virtual ApplicationUser myResource { get; set; }
        public virtual Project myProject { get; set; }
    }
}
