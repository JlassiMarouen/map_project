﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using MAP_project.domaine.Entity;
using System.Data.Entity;

namespace MAP_project.domaine.Entity
{
   public class ApplicationUser : IdentityUser
    {
     

        //public int UserId { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
     //   [DataType(DataType.EmailAddress)]

       // public string email { get; set; }
        //public string login { get; set; }

        [Required(ErrorMessage ="obligatoire")]
        [StringLength(15,ErrorMessage ="La chaine ne doit pas dépasser 15 caractéres")]
        [MinLength(5)]
        public string password { get; set; }

        // public int role { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

         public DbSet<Resource> MyRessource { get; set; }
        ApplicationUser appUser = new ApplicationUser();
        public DbSet<Project> MyProjects { get; set; }
        public DbSet<Skils> MySkils { get; set; }
    }


}
