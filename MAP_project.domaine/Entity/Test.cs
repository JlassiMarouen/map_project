﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace MAP_project.domaine.Entity
{
    public class Test
    {
     
        public int TestId { get; set; }
        public StateTestT stateTest { get; set; }
       // [DataType(DataType.Date)]
        public DateTime date { get; set; }
        public TestTypeT testType { get; set; }
        public TestResultT TestResult { get; set; }

    }
}
