﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MAP_project.domaine.Entity
{
   public class Organigram
    {
      
        public int OrganigramId { get; set; }
        public string nameProgram { get; set; }
        public string nameCh { get; set; }
        public string nameProject { get; set; }
        public string nameClient { get; set; }
        public string accountManager { get; set; }
        public string nameAssignementM { get; set; }

        public virtual ICollection<Resource>  MyResouces { get; set; }

        

    }
}
