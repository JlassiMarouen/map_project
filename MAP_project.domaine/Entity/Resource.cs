﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MAP_project.domaine.Entity
{
   public class Resource :ApplicationUser
    {
      
   // public int ResourceId { get; set; }
        public string picture { get; set; }
        
        public string profile { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "valeur doit etre positive")]
        public int seniority { get; set; }
        public string note { get; set; }
        public Contract contract { get; set; }
        public string sector { get; set; }
        public StateRes state { get; set; }
        [ForeignKey ("MyProject")]
        public string MprojectId { get; set; }
        public string cv { get; set; }
        public string origine { get; set; }
        public virtual Organigram myOrganigram { get; set; }
        public virtual ICollection<Project> MyProject { get; set; }
        public virtual ICollection<Message> MyMessage { get; set; }
        public virtual ResourceManager MyResourceManager { get; set; }




    }
}
