﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MAP_project.domaine.Entity
{
   public class Client: ApplicationUser
    {
       
       // public string name { get; set; }
        public CategoryT category_type { get; set; }
        public string logo { get; set; }
        public ClientT client_type { get; set; }

        public virtual  ICollection<Project> MyProjects { get; set; }
        public virtual ICollection<Message> MyMessages { get; set; }
        public virtual ResourceRequest MyResourceRequestes { get; set; }



    }
}
