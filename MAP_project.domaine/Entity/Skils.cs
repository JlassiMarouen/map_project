﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MAP_project.domaine.Entity
{
    public class Skils
    {
     
        public int SkilsId { get; set; }
        public int idRes { get; set; }
        public int idProject { get; set; }
        [DataType(DataType.Text)]
        public string value { get; set; }
    }
}
