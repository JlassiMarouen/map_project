﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace MAP_project.domaine.Entity
{
    public class ResourceRequest
    {
        public string ResourceRequestId { get; set; }

        [ForeignKey("MyClientRR")]
        public string clientRRId { get; set; }
        public StateReq state_type { get; set; }
        public string resourceType { get; set; }
        public string Duration { get; set; }
        public float coast { get; set; }
        public string project { get; set; }
       public virtual ApplicationUser MyClientRR { get; set; }
    }
}
