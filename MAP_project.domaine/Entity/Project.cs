﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MAP_project.domaine.Entity
{
   public class Project
    {
        
        public int ProjectId { get; set; }
        public string type { get; set; }

        [DataType(DataType.Date)]
        public DateTime deb_Date { get; set; }

       [DataType(DataType.Date)]
        public DateTime fin_Date { get; set; }
        public int NbreRessources { get; set; }

        [ForeignKey("myClientP")]
        public string idClient { get; set; }
        public virtual ApplicationUser myClientP { get; set; }
        public virtual Organigram myOrganigram { get; set; }
        public virtual ICollection<Resource> MyResources { get; set; }
    }
}
