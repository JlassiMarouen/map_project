﻿using MAP_project.domaine.Entity;
using MAP_project.pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MAP_project.data.Infrastructure;

namespace MAP_project.service
{
    public class GestionResource : GestionService<Resource>, IGestionRessource
    {
        private static DataBaseFactory dbFactory = new DataBaseFactory();
        private static UnitOfWork utw = new UnitOfWork(dbFactory);
        public GestionResource() : base(utw)
        {

        }
    }
}
